# Task

Your task is to implement the method `isLeap` in the class `LeapYear`.

# Specification

The method `isLeap` determines if a given year is a leap year or not.

# Algorithm

A leap year is a year that is divisible by `4` except if the year is divisible
by `100` then the year is not a leap year except if the year is divisible by
`400` then the year is a leap year.

# Support

To support you with implementing the method there are unit tests in
`LeapYearTest`. The unit tests are disabled at the start. To use them you have
to enable them.

# Development Requirements

To perform this exercise you need a JDK 8 and Maven 3.3.x.

# References

* [Rosetta Code][rcly]

[rcly]: https://rosettacode.org/wiki/Leap_year "Rosetta Code - Leap year"
