package net.sw4j.javadojo.leapyear;

/**
 * This class has a method to determine if a year is a leap year.
 */
public class LeapYear {

    /**
     * Determines if a given year is a leap year or not.
     *
     * @param year the year to check.
     * @return {@code true} if the given year is a leap year.
     */
    public boolean isLeap(int year) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
